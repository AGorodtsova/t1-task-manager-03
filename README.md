# TASK MANAGER

## DEVELOPER INFO

**NAME**: Anastasia Gorodtsova

**E-MAIL**: agorodtsova@t1-consulting.ru

## SOFRWARE

**OS**: Windows 10 Pro 21H2

**JDK**: OPENJDK 1.8.0_322

## HARDWARE

**CPU**: i7-10700KF

**RAM**: 16GB

**SSD**: 256GB

## RUN PROGRAM

```
java -jar ./task-manager.jar
```
